﻿using UnityEngine;
using System.Collections;

public class EnemySpawningScript : MonoBehaviour {
        public float timer = 0.0f;
        public bool spawning = false;
        public Rigidbody prefab;
        public Rigidbody enemyBlue;
        public Rigidbody enemyRed;
        public Rigidbody enemyGreen;
        public int delay;
        public int speed;
        public Rigidbody target;

        private float rightMost, leftMost, upMost, downMost;
        private float tempX, tempZ;
        private float frustumHeight, frustumWidth;


        //TODO: DE-hardcode :P
        private const float DISTANCE_FROM_CAMERA = 15f;
        private const float CENTER_X = 0.0f;
        private const float CENTER_Z = 0.0f;
        private const float GAP = 1.0f;
        
        Camera camera;

	// Use this for initialization
	void Start ()
    {
        //Put in the Start assuming the camera won't change during the game session
        camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //check if spawning at the moment, if not add to timer
        if (!spawning)
        {
            timer += Time.deltaTime;
        }
        //when timer reaches "delay" seconds, call Spawn function
        if (timer >= delay)
        {
            Spawn();
        }
	}

    private void InitiateScreenVariables()
    {
        //These formula give the frustum height and width at a specific DISTANCE_FROM_CAMERA
        //Found in Unity's documentation
        frustumHeight = 2.0f * DISTANCE_FROM_CAMERA * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        frustumWidth = frustumHeight * camera.aspect;

        //Using X and Z for topDown view
        tempX = 0.0f;
        tempZ = 0.0f;

        rightMost = CENTER_X + (frustumWidth / 2) + GAP;
        leftMost = CENTER_X - (frustumWidth / 2) - GAP;
        upMost = CENTER_Z + (frustumHeight / 2) + GAP;
        downMost = CENTER_Z - (frustumHeight / 2) - GAP;
    }

    public void Spawn()
    {
        spawning = true;
        timer = 0;



        GameObject[] resourcesOnScreen;
        resourcesOnScreen = GameObject.FindGameObjectsWithTag("resource");

        //Spawns only if there's a resource on screen
        if(resourcesOnScreen.Length > 0)
        {
            int whichResource = UnityEngine.Random.Range(0, resourcesOnScreen.Length);
            Color whichColor = resourcesOnScreen[whichResource].GetComponent<Renderer>().material.color;

            if (whichColor == Color.blue)
                prefab = enemyBlue;
            else if (whichColor == Color.red)
                prefab = enemyRed;
            else if (whichColor == Color.green)
                prefab = enemyGreen;
            else
                Debug.Log("Trouble with resource color");

            Vector3 tempPosition = GenerateOffscreenSpawn();
            Rigidbody spawnedObject = (Rigidbody)Instantiate(prefab, tempPosition, transform.rotation);

            Vector3 targetDirection = target.position - spawnedObject.transform.position;

            //Rotation happens here
            spawnedObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            spawnedObject.transform.rotation = Quaternion.LookRotation(targetDirection * -1);

            spawnedObject.AddForce(targetDirection * speed);
        }


        //halt script for "delay" second before returning to the start of the process
        StartCoroutine(Wait(delay));
        spawning = false;
    }

    IEnumerator Wait(int TimeInSeconds)
    {
        yield return new WaitForSeconds(TimeInSeconds);
    }

    //Generates a random spawn location outside the screen
    //using camera's frustum
    // * NOTE: The Qty of spawns generated for each side
    // *    of the screen will roughly be the same, meaning
    // *    the qty of generated spawns is not proportional
    // *    to the screen size
    Vector3 GenerateOffscreenSpawn()
    {
        //This is done here in case the window have been
        //resized by the player
        //TODO: Try to find a event for window resize
        InitiateScreenVariables();

        //random.range second parameter is exlusive
        //even if the doc says it's inclusive
        int side = UnityEngine.Random.Range(0, 4);

        switch(side)
        {
            case 0: //UP
                tempZ = upMost;
                tempX = UnityEngine.Random.Range(leftMost, rightMost);
                break;
            case 1: //DOWN
                tempZ = downMost;
                tempX = UnityEngine.Random.Range(leftMost, rightMost);
                break;
            case 2: //LEFT
                tempX = leftMost;
                tempZ = UnityEngine.Random.Range(downMost, upMost);
                break;
            case 3: //RIGHT
                tempX = rightMost;
                tempZ = UnityEngine.Random.Range(downMost, upMost);
                break;
        }


        //Tried WorldToViewportPoint and others to make it work with Screen.Width and Screen.Height
        //but the best way to know if something is in the user's field of view
        //is by using frustum
        return new Vector3(tempX, 0f, tempZ);
    }
}
