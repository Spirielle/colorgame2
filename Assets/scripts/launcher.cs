﻿using UnityEngine;
using System.Collections;

public class launcher : MonoBehaviour {

	public GameObject Bullet;
	public float maxEnergy = 5000;
	private float boosting = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("return")) {
			if (boosting < maxEnergy){
				boosting = boosting + 10;
				GetComponent<Renderer>().material.color = Color.Lerp(Color.black, Color.red, Time.deltaTime * (boosting/100));
			}
				}
		if (Input.GetKeyUp("return")) {
			GameObject BulletInstance = Instantiate(Bullet, transform.position, transform.rotation) as GameObject;
			BulletInstance.GetComponent<Rigidbody>().AddForce(transform.up * (100 + boosting));
			boosting = 0;
			GetComponent<Renderer>().material.color = Color.black;
				}
		Debug.Log(boosting);
		}
	
	}

