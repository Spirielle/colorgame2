﻿using UnityEngine;
using System.Collections;

public class selfDestroy : MonoBehaviour {
    playExplosion playerScript;
	
	// Use this for initialization
	void Start () {
        GameObject thePlayer = GameObject.Find("SoundEmitter");
        playerScript = thePlayer.GetComponent<playExplosion>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "bullet")
        {
            GameObject power = GameObject.Find("powerCore");
            if (power.GetComponent<Renderer>().material.color == GetComponentInChildren<Renderer>().material.color)
            {
                playerScript.play();
                Destroy(gameObject);
            }
        }
        else if(collider.gameObject.tag == "ship")
        {
            Destroy(gameObject);
        }
    }
}
