﻿using UnityEngine;
using System.Collections;

public class resourceDestroy2 : MonoBehaviour {

    playExplosion playerScript;

	// Use this for initialization
	void Start () {
        GameObject thePlayer = GameObject.Find("SoundEmitter");
        playerScript = thePlayer.GetComponent<playExplosion>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        playerScript.play();
        Destroy(gameObject);
    }
}
