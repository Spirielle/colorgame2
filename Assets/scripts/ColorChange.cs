﻿using UnityEngine;
using System.Collections;

public class ColorChange : MonoBehaviour {

	public AudioClip resourcecollect;
	AudioSource audio;
	
	void Start () {
		audio = GetComponent<AudioSource>();

	}

	void OnTriggerEnter(Collider collider){

		if (collider.gameObject.tag == "resource") {

			GameObject cube = collider.gameObject;
			GameObject power = GameObject.Find ("powerCore");
			power.GetComponent<Renderer> ().material.color = cube.GetComponent<Renderer> ().material.color;
		
			audio.PlayOneShot(resourcecollect);

	}
}
}