﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeScript : MonoBehaviour {

    public static int life = 5;
    public Text lifeText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void DeadScreen()
    {
        GameObject camera = GameObject.Find("Main Camera");
        EnemySpawningScript spawningScript = camera.GetComponent<EnemySpawningScript>();
        spawningScript.speed = 5000;
        spawningScript.delay = 0;
    }
  

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "enemy")
        {
            Destroy(collider.gameObject);
            life--;

            switch (life)
            {
                case 0:
                    lifeText.text = "YOU ARE DEAD";
                    DeadScreen();
                    break;
                case 1:
                    lifeText.text = "❤";
                    break;
                case 2:
                    lifeText.text = "❤❤";
                    break;
                case 3:
                    lifeText.text = "❤❤❤";
                    break;
                case 4:
                    lifeText.text = "❤❤❤❤";
                    break;
                case 5:
                    lifeText.text = "❤❤❤❤❤";
                    break;
            }
        }

    }

}
