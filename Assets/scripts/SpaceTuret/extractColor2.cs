﻿using UnityEngine;
using System.Collections;

public class extractColor2 : MonoBehaviour {

	public AudioClip grab;
	AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("down")) {
                    transform.localScale += new Vector3(0, 0.2f, 0);
                    transform.Translate(new Vector3(0, 0.08f, 0));
                        if(!audio.isPlaying)
                             audio.PlayOneShot(grab);
				}
			
				if (Input.GetKeyUp ("down")) {
                    //1.58 is the original x position
                    transform.localPosition = new Vector3(1.58f, 0, 0);
                    //0.78 is the original y scale
                    transform.localScale = new Vector3(transform.localScale.x, 0.78f, transform.localScale.z);
				}
		GameObject power = GameObject.Find ("powerCore");
		GetComponent<Renderer>().material.color = power.GetComponent<Renderer> ().material.color;
	}
}
