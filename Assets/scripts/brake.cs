﻿using UnityEngine;
using System.Collections;

public class brake : MonoBehaviour {

	public GameObject broken;

	// Use this for initialization
	void Start () {
	
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "bullet") {
			Instantiate(broken, transform.position, transform.rotation);
			Destroy (gameObject);
				}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
