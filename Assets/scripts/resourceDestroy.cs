﻿using UnityEngine;
using System.Collections;

public class resourcefDestroy : MonoBehaviour
{
    playExplosion playerScript;

    // Use this for initialization
    void Start()
    {
        GameObject thePlayer = GameObject.Find("SoundEmitter");
        playerScript = thePlayer.GetComponent<playExplosion>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //This works like onCollision without applying physic
    // Perfect for bullet as we don't want to affect 
    // enemy trajectory with them
    void OnTriggerEnter(Collider collider)
    {
        playerScript.play();
        Destroy(gameObject);
    }
}
