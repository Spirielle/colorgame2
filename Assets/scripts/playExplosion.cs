﻿using UnityEngine;
using System.Collections;

public class playExplosion : MonoBehaviour
{


    public AudioClip shoot;
    AudioSource audio;

    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void play()
    {
        audio.PlayOneShot(shoot);
    }
}

